 ```sql
 CREATE TABLE `pnc_vendor` (
  `vendor_id` int(11) NOT NULL AUTO_INCREMENT,
  `account_num` varchar(17) DEFAULT NULL,
  `routing_num` varchar(10) DEFAULT NULL,
  `created_date` datetime  NOT NULL,
  `updated_date` datetime  NULL,
  `related_vendor_id` varchar(45) NOT NULL,
  PRIMARY KEY (`vendor_id`),
  UNIQUE KEY `vendor_id_UNIQUE` (`vendor_id`),
  UNIQUE KEY `related_vendor_id_UNIQUE` (`related_vendor_id`),
  UNIQUE INDEX `vendor_id_UNIQUE_3` (`vendor_id` ASC),
  UNIQUE INDEX `related_vendor_id_UNIQUE_3` (`related_vendor_id` ASC)
)

DROP TABLE payment;

CREATE TABLE payment (
	payment_id int(11) NOT NULL AUTO_INCREMENT, 
    account_num varchar(20) NOT NULL, 
	routing_num varchar(20) NOT NULL, 
    account_name varchar(20) NOT NULL, 
    account_type varchar(20) NOT NULL,
    
    currency varchar(20) NOT NULL,
    amount decimal(15,2) NOT NULL,
    payment_status varchar(20) NULL, 
    payment_error varchar(200) NULL,
	client_uuid varchar(50) NOT NULL, 
	server_uuid varchar(50) NOT NULL, 
	client_notes varchar(200) DEFAULT NULL, 
	server_notes varchar(200) DEFAULT NULL,
	erp_vendor_id varchar(50) NOT NULL, 
	vendor_id int(11) NULL, 
	vendor_name varchar(50) NOT NULL,
	initiator_id varchar(20) DEFAULT NULL,
	erp_system varchar(20) NOT NULL,
	posted_timestamp datetime NOT NULL,
	created_timestamp datetime NOT NULL, 
    updated_timestamp datetime NULL, 
    processed_timestamp datetime NULL,
	PRIMARY KEY (payment_id), 
    UNIQUE KEY payment_id_UNIQUE (payment_id), 
    UNIQUE INDEX payment_id_UNIQUE_3 (payment_id ASC));
    
SELECT * FROM payment;
```

java -Xdebug -Xrunjdwp:transport=dt_socket,server=y,address=8001,suspend=y -jar 