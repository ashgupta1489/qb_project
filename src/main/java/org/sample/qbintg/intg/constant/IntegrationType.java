package org.sample.qbintg.intg.constant;

public enum IntegrationType {

	NETSUITE,
	QUICKBOOKS,
	XERO;

}

