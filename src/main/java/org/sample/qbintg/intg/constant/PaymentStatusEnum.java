package org.sample.qbintg.intg.constant;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;


public enum PaymentStatusEnum {

	SUCCESSFUL("Successful",80), FAILED("Failed",5), IN_PROGRESS("Processing",40), VOIDED("Voided",5), REJECTED("Rejected",5);

	private final String val;
	private int weight;

	PaymentStatusEnum(String v, int w) {
		val = v;
		this.weight = w;
	}
	public String val() {
		return val;
	}

	private int getWeight() {
		return weight;
	}



	private static final List<PaymentStatusEnum> VALUES =  Collections.unmodifiableList(Arrays.asList(values()));

	private static int summWeigts() {
		int summ = 0;
		for (PaymentStatusEnum value : VALUES) {
			summ += value.getWeight();
		}
		return summ;
	}


	private static final int SIZE = summWeigts();
	private static final Random RANDOM = new Random();


	public static PaymentStatusEnum randomPaymentStat()  {
		//return VALUES.get(RANDOM.nextInt(SIZE));
		PaymentStatusEnum returnVal = null;
		int randomNum = RANDOM.nextInt(SIZE);
		int currentWeightSumm = 0;
		for(PaymentStatusEnum currentValue: VALUES) {
			returnVal = currentValue;
			if (randomNum > currentWeightSumm && randomNum <= (currentWeightSumm + currentValue.getWeight())) {
				break;
			}
			currentWeightSumm += currentValue.getWeight();
		}
		return returnVal;
	

	}

}
