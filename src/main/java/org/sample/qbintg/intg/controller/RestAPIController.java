package org.sample.qbintg.intg.controller;

import static org.sample.qbintg.intg.constant.MessageEnum.BEARER_ERROR;
import static org.sample.qbintg.intg.constant.MessageEnum.CONNECTION_INCOMPLETE;
import static org.sample.qbintg.intg.constant.MessageEnum.GENERIC_ERROR;
import static org.sample.qbintg.intg.constant.MessageEnum.REFRESH_ERROR;
import static org.sample.qbintg.intg.constant.MessageEnum.RETRIEVE_NS_BILLPAYMENT_ERROR;
import static org.sample.qbintg.intg.constant.MessageEnum.SETUP_ERROR;
import static org.sample.qbintg.intg.constant.MessageEnum.UNAUTH_ERROR;
import static org.sample.qbintg.intg.constant.NetSuiteDataType.NS_BILL;
import static org.sample.qbintg.intg.constant.NetSuiteDataType.NS_VENDOR;
import static org.sample.qbintg.intg.constant.QuickBooksDataType.BILL;
import static org.sample.qbintg.intg.constant.QuickBooksDataType.BILL_PAYMENT;
import static org.sample.qbintg.intg.constant.QuickBooksDataType.VENDOR;
import static org.sample.qbintg.intg.util.Utilities.getMessage;
import static org.sample.qbintg.intg.util.Utilities.getResultJsonString;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.sample.qbintg.OAuth2PlatformClientFactory;
import org.sample.qbintg.intg.constant.IntegrationType;
import org.sample.qbintg.intg.constant.MessageEnum;
import org.sample.qbintg.intg.constant.NetSuiteDataType;
import org.sample.qbintg.intg.constant.PaymentStatusEnum;
import org.sample.qbintg.intg.constant.QuickBooksDataType;
import org.sample.qbintg.intg.helper.DataHelper;
import org.sample.qbintg.intg.model.BillPaymentModelView;
import org.sample.qbintg.intg.model.MakePaymentModalView;
import org.sample.qbintg.intg.model.NSBillPayment;
import org.sample.qbintg.intg.model.NsMakePayment;
import org.sample.qbintg.intg.model.PaymentResponse;
import org.sample.qbintg.intg.model.VendorDto;
import org.sample.qbintg.intg.model.VendorPayment;
import org.sample.qbintg.repository.IDataRepository;
import org.sample.qbintg.repository.VendorDataRepository;
import org.sample.qbintg.repository.VendorPaymentDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.scribejava.core.builder.api.DefaultApi10a;
import com.github.scribejava.core.model.OAuth1AccessToken;
import com.github.scribejava.core.model.OAuth1RequestToken;
import com.github.scribejava.core.model.OAuthConfig;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth10aService;
import com.github.scribejava.core.utils.StreamUtils;
import com.intuit.ipp.core.Context;
import com.intuit.ipp.core.ServiceType;
import com.intuit.ipp.data.Error;
import com.intuit.ipp.exception.FMSException;
import com.intuit.ipp.exception.InvalidTokenException;
import com.intuit.ipp.security.OAuth2Authorizer;
import com.intuit.ipp.services.DataService;
import com.intuit.ipp.services.QueryResult;
import com.intuit.ipp.util.Config;
import com.intuit.oauth2.client.OAuth2PlatformClient;
import com.intuit.oauth2.data.BearerTokenResponse;
import com.intuit.oauth2.exception.OAuthException;

@RestController
public class RestAPIController {

	private static final Logger logger = Logger.getLogger(RestAPIController.class);

	@Autowired
	private OAuth2PlatformClientFactory factory;

	@Autowired
	private VendorDataRepository vendorDataRepository;

	@Autowired
	private VendorPaymentDataRepository vendorPaymentDataRepository;

	private DataService dataService;
	private final static  int scriptNum = 640;
	private   final static  int deployNum =1;
	private static ObjectMapper mapper = new ObjectMapper();

	@Autowired
	private Environment env;

	@RequestMapping("/health")
	public String checkHealth(){
		return "Working fine";
	}


	//Throttling can be implemented here
	@ResponseBody
	@RequestMapping("/getBills")
	public String getBills(HttpSession session) {
		String quickBooksData =  getData(session, BILL);
		String netSuiteData = getNetSuiteData(NS_BILL);
		String returnStr = "{\"quickbooks\":"+quickBooksData+",\"netsuite\":"+netSuiteData+"}";
		return returnStr;
	}


	@ResponseBody
	@RequestMapping("/getBillPayments")
	public String getBillPayments(HttpSession session) {
		String quickBooksData =  getData(session, BILL_PAYMENT);
		String netSuiteData = getNetSuitePayments();//getMessage(NS_BILL_PAYMENT.getFailureMessage());  // 
		String returnStr = "{\"quickbooks\":"+quickBooksData+",\"netsuite\":"+netSuiteData+"}";
		return returnStr;
	}


	@ResponseBody
	@RequestMapping("/getVendors")
	public String getVendors(HttpSession session) {
		String quickBooksData =  getData(session, VENDOR);
		String netSuiteData = getNetSuiteData(NS_VENDOR);
		String returnStr = "{\"quickbooks\":"+quickBooksData+",\"netsuite\":"+netSuiteData+"}";
		return returnStr;
	}




	@ResponseBody
	@RequestMapping(value = "/makePayment", method = RequestMethod.POST)
	public String makePayment(HttpSession session, @RequestBody MakePaymentModalView makePayment) {
		logger.info("Inside Payment object");
		String failureMsg = validateConfig(session);
		boolean isPaid = false;
		if(!StringUtils.isEmpty(failureMsg)){
			return failureMsg;
		}
		try{
			if(makePayment!=null && makePayment.getSource()!=null && makePayment.getSource().equalsIgnoreCase("Pulled from NetSuite")) {
				isPaid = createNsBillPayment(makePayment);
			}
			else {
				isPaid= DataHelper.createBillPayment(dataService,makePayment);

			}			}
		catch(Exception e){
			return MessageEnum.UNABLE_PAYMENT_ERROR.desc();
		}
		logger.info("Bill Paid : " + isPaid);
		return "Payment Completed";
	}

	@ResponseBody
	@RequestMapping(value = "/saveVendor", method = RequestMethod.POST)
	public String saveVendor(HttpSession session, @RequestBody VendorDto vendorDto) {
		logger.info("Inside Vendor save method ");
		String failureMsg = validateConfig(session);
		boolean isSaved = false;
		if(!StringUtils.isEmpty(failureMsg)){
			return failureMsg;
		}
		try{
			isSaved= DataHelper.saveVendor(dataService,vendorDataRepository,vendorDto);
		}
		catch(Exception e){
			return MessageEnum.UNABLE_PAYMENT_ERROR.desc();
		}
		logger.info("Vendor Saved " + isSaved);
		//session.setAttribute("billUpdated", true);
		return "Payment Completed";
	}


	// consumes="application/json"
	@RequestMapping(value = "/payVendor", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<PaymentResponse> payVendor(@RequestBody VendorPayment vendorPayment) throws Exception{
		System.out.println("Inside vendor method");
		logger.info("Inside payVendor method");
		if(vendorPayment==null) {
			logger.error("Null Request");
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
		logger.info(""+vendorPayment.toString());

		if( StringUtils.isEmpty(vendorPayment.getAccountNumber())
				|| StringUtils.isEmpty(vendorPayment.getRoutingNumber()) || StringUtils.isEmpty(vendorPayment.getAccountName())
				|| StringUtils.isEmpty(vendorPayment.getClientUUID())|| StringUtils.isEmpty(vendorPayment.getCurrency())
				|| !NumberUtils.isNumber(vendorPayment.getAccountNumber()) || !NumberUtils.isNumber(vendorPayment.getRoutingNumber())
				|| vendorPayment.getAmount()==null ||  vendorPayment.getAmount().compareTo(BigDecimal.ZERO)<=0) {

			PaymentResponse paymentResponse = new PaymentResponse();
			paymentResponse.setServerUUID(null);
			paymentResponse.setServerTimestamp(LocalDateTime.now().toString());
			paymentResponse.setError("Mandatory field(s) are not valid");
			return new ResponseEntity<PaymentResponse>(paymentResponse, HttpStatus.BAD_REQUEST);
		}

		VendorPayment dbPayment = null;
		//In prod, we would store each record , rather than updating it

		if(!StringUtils.isEmpty(vendorPayment.getServerUUID())){
			try{		
				dbPayment= vendorPaymentDataRepository.findByServerUUID(vendorPayment.getServerUUID());  
				logger.info("Matching Record Identified. Request recieved for same payment. Not duplicating others");
				if(dbPayment!=null && dbPayment.getServerUUID()!=null && dbPayment.getId()>0) {
					vendorPayment = dbPayment;
					vendorPayment.setServerNotes("Payment Request Re-initaited.");
				}
			}
			catch(Exception e){
				logger.error("Error while saving",e);
			}
		}
		else {
			vendorPayment.setAccountType("RTP");
			vendorPayment.setServerUUID(UUID.randomUUID().toString());
			vendorPayment.setErpSystem(IntegrationType.NETSUITE.toString());
			vendorPayment.setCreatedTimestamp(LocalDateTime.now());
			vendorPayment.setServerNotes("Recieved the Payment Request for vendor: "+ vendorPayment.getVendorName()+" for payment of  : "+ vendorPayment.getCurrency().toUpperCase()+" "+vendorPayment.getAmount());
			long timestamp = Long.parseLong(vendorPayment.getPostedTimeStampUnit());
			//vendorPayment.setPostedTimeStamp(new Date(timestamp * 1000));
			vendorPayment.setPostedTimeStamp(LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp), ZoneId.systemDefault()));
		}

		vendorPayment.setPostedAmount(vendorPayment.getAmount()); // In mvp, it wouldn't
		vendorPayment.setStatus(PaymentStatusEnum.SUCCESSFUL.val());

		try{
			vendorPayment= vendorPaymentDataRepository.saveAndFlush(vendorPayment);
		}
		catch(Exception e){
			logger.error("Error while saving",e);
		}
		if(vendorPayment == null || vendorPayment.getId()<=0) {
			PaymentResponse paymentResponse = new PaymentResponse();
			paymentResponse.setServerUUID(null);
			paymentResponse.setServerTimestamp(LocalDateTime.now().toString());
			paymentResponse.setError("Unable to Process the Request");
			return new ResponseEntity<PaymentResponse>(paymentResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info(""+vendorPayment.toString());
		PaymentResponse paymentResponse = new PaymentResponse(vendorPayment);


		return new ResponseEntity<PaymentResponse>(paymentResponse, HttpStatus.OK);

	}







	@RequestMapping(value = "/paymentstatus/{uuid}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<PaymentResponse> getStatus(@PathVariable("uuid")  String uuid) throws Exception {
		logger.info("Inside Payment Status method for particular transaction " +uuid);
		if(StringUtils.isEmpty(uuid)) {
			return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
		}
		VendorPayment vendorPayment = null;
		try{
			vendorPayment= vendorPaymentDataRepository.findByServerUUID(uuid);  
		}
		catch(Exception e){
			logger.error("Error while saving",e);
		}
		if(vendorPayment==null || vendorPayment.getId()<=0) {
			PaymentResponse paymentResponse = new PaymentResponse();
			//	paymentResponse.setServerUUID(uuid);
			paymentResponse.setServerTimestamp(LocalDateTime.now().toString());
			paymentResponse.setError("Invalid Request. No Data Found");
			return new ResponseEntity<PaymentResponse>(paymentResponse, HttpStatus.BAD_REQUEST);
		}
		else {
			//logger.info(vendorPayment.toString());
			// based on processing 
			if(vendorPayment.getStatus()!=null && vendorPayment.getStatus().equalsIgnoreCase(PaymentStatusEnum.IN_PROGRESS.val())) {
				PaymentStatusEnum statusEnum = PaymentStatusEnum.randomPaymentStat();
				vendorPayment.setStatus(statusEnum.val());
				if(statusEnum!=PaymentStatusEnum.IN_PROGRESS) {
					vendorPayment.setServerNotes("PNC has processed your payment as:: "+ statusEnum.val());
					vendorPayment.setProcessedTimeStamp(LocalDateTime.now());
					vendorPaymentDataRepository.saveAndFlush(vendorPayment);
				}
				else {
					vendorPayment.setServerNotes("PNC is still processing your payment");
				}
			}
			//logger.info(vendorPayment.toString());
			PaymentResponse paymentResponse = new PaymentResponse(vendorPayment);
			logger.info(""+paymentResponse.toString());
			return new ResponseEntity<PaymentResponse>(paymentResponse, HttpStatus.OK);
		}

	}


	private String validateConfig(HttpSession session){
		String failureMsg="";
		try {
			String realmId = (String)session.getAttribute("realmId");
			if (StringUtils.isEmpty(realmId)) {
				failureMsg =getMessage(CONNECTION_INCOMPLETE);
			}
			String accessToken = (String)session.getAttribute("access_token");
			String url = factory.getPropertyValue("IntuitAccountingAPIHost") + "/v3/company";
			Config.setProperty(Config.BASE_URL_QBO, url);
			getDataService(realmId, accessToken);
		}
		catch (FMSException e) {
			List<Error> list = e.getErrorList();
			list.forEach(error -> logger.error(SETUP_ERROR.desc() + error.getMessage()));
			return getMessage(CONNECTION_INCOMPLETE);
		}
		catch(Exception e){
			logger.error(GENERIC_ERROR.desc(),e);
			return getMessage(GENERIC_ERROR);
		}
		return failureMsg;
	}


	private void getDataService(String realmId, String accessToken) throws FMSException {
		OAuth2Authorizer oauth = new OAuth2Authorizer(accessToken);
		Context context = new Context(oauth, ServiceType.QBO, realmId); 
		dataService =  new DataService(context);
	}


	/*
	 * This method is called to handle 401 status code - 
	 * If a 401 response is received, refresh tokens should be used to get a new access token,
	 * and the API call should be tried again.
	 */

	private String  refreshTokens(HttpSession session) throws Exception {
		try{
			OAuth2PlatformClient client  = factory.getOAuth2PlatformClient();
			String refreshToken = (String)session.getAttribute("refresh_token");
			BearerTokenResponse bearerTokenResponse = client.refreshToken(refreshToken);
			session.setAttribute("access_token", bearerTokenResponse.getAccessToken());
			session.setAttribute("refresh_token", bearerTokenResponse.getRefreshToken());
			getDataService((String)session.getAttribute("realmId"), bearerTokenResponse.getAccessToken());
			return "";
		}
		catch (OAuthException e1) {
			logger.error(BEARER_ERROR.desc() + e1.getMessage());
			return getMessage(REFRESH_ERROR);
		}
		catch (FMSException e) {
			List<Error> list = e.getErrorList();
			list.forEach(error -> logger.error(SETUP_ERROR.desc() + error.getMessage()));
			return getMessage(CONNECTION_INCOMPLETE);
		}

	}



	private String getData(HttpSession session, QuickBooksDataType dataType){
		MessageEnum messageEnum = dataType.getFailureMessage();
		String failureMsg=messageEnum.desc();
		String sql = dataType.getSql();
		IDataRepository dataRepository = null;
		if(dataType==VENDOR){
			dataRepository = vendorDataRepository;
		}

		try {
			failureMsg = validateConfig(session);
			if(!StringUtils.isEmpty(failureMsg)){
				return failureMsg;
			}
			QueryResult queryResult = dataService.executeQuery(sql);
			return DataHelper.processResponseForType(queryResult,dataType,dataRepository);
		}
		catch (InvalidTokenException e) {			
			logger.error(failureMsg + e.getMessage());
			logger.info(UNAUTH_ERROR.desc());
			try {
				refreshTokens(session);
				logger.info("Calling data service again");
				QueryResult queryResult = dataService.executeQuery(sql);
				return  DataHelper.processResponseForType(queryResult,dataType,dataRepository);
			} 
			catch (FMSException e2) {
				logger.error(failureMsg + e2.getMessage());
				return getMessage(messageEnum);
			}
			catch (Exception e1) {
				logger.error("Retry failed with fresh token" + e.getMessage());
				return getMessage(messageEnum);
			}

		} 
		catch (FMSException e2) {
			logger.error(failureMsg + e2.getMessage());
			return getMessage(messageEnum);
		}
		catch (Exception e1) {
			logger.error(failureMsg + e1.getMessage());
			return getMessage(messageEnum);
		}
	}


	/************************************************************************************************************************************************************************************/
	
	private List<BillPaymentModelView> getDataPushedFromNS() {
		List<VendorPayment> vendorPayments = new ArrayList<>();
		List<BillPaymentModelView> views = new ArrayList<>();
		try{
			vendorPayments= vendorPaymentDataRepository.findAllByOrderByIdAsc();
			
			if(!CollectionUtils.isEmpty(vendorPayments)) {
				for (VendorPayment vendorPayment : vendorPayments) {
					views.add(new BillPaymentModelView((vendorPayment)));
				}
			}
		}
		catch(Exception e){
			logger.error("Error while retrieving",e);
		}
		return views;

	}
	
	
	private String getNetSuitePayments() {
		List<BillPaymentModelView> views = new ArrayList<>();
		views.addAll(getDataPushedFromNS());
		views.addAll(getPaymentNetSuiteData());
		return getResultJsonString(views);
	}


	

	private List<BillPaymentModelView> processNSBillPaymentResponse(String body) {
		List<BillPaymentModelView> views = new ArrayList<>();
		if(!StringUtils.isEmpty(body)) {
			try {
				List<NSBillPayment> payments = mapper.readValue(body, mapper.getTypeFactory().constructCollectionType(List.class, NSBillPayment.class));
				for (Iterator<NSBillPayment> iterator = payments.iterator(); iterator.hasNext();) {
					NSBillPayment nsPayment = (NSBillPayment) iterator.next();
					if(!StringUtils.isNotEmpty(nsPayment.getData().getAmountPaid()) || nsPayment.getData().getAmountPaid().equalsIgnoreCase("0.00")
							|| StringUtils.isEmpty(nsPayment.getData().getVendorName())
							||nsPayment.getData().getAmountPaid().equalsIgnoreCase(".00")) {
						iterator.remove();
					}
				}
				if (!payments.isEmpty() && payments.size() > 0) {
					payments.stream().forEach(record -> views.add(new BillPaymentModelView(record)));
				} 
			}
			catch (Exception e) {
				logger.error(RETRIEVE_NS_BILLPAYMENT_ERROR.desc(), e);
			}
		}
		return views;
	}

	private  List<BillPaymentModelView> getPaymentNetSuiteData(){
		OAuthConfig authConfig = new OAuthConfig(env.getProperty("netsuite.consumer.key"), env.getProperty("netsuite.consumer.secret"));
		OAuth1AccessToken token = new OAuth1AccessToken(env.getProperty("netsuite.token.key"), env.getProperty("netsuite.token.secret"));
		String url  = env.getProperty("netsuite.baseurl")+"script="+scriptNum+"&deploy="+deployNum+"&recordtype="+NetSuiteDataType.NS_BILL_PAYMENT.getRecordName()+"&id="+0+"&isMultiple="+true;
		try (OAuth10aService auth10aService = new OAuth10aService(new NSApi(), authConfig)) {
			OAuthRequest request = new OAuthRequest(Verb.GET, url);
			request.setRealm("TSTDRV1798497");
			request.addHeader("Content-Type", "application/json");
			request.addHeader("Accept", "application/json");
			auth10aService.signRequest(token, request);
			Response response = auth10aService.execute(request);
			String body = StreamUtils.getStreamContents(response.getStream());
			logger.info(body);
			return  processNSBillPaymentResponse(body);
		}
		catch(Exception e) {
			logger.error("Error Calling NetSuite API",e);
		}
		return new ArrayList<BillPaymentModelView>();
	}





	private  String getNetSuiteData(NetSuiteDataType dataType){
		MessageEnum failureMessage = dataType.getFailureMessage();
		OAuthConfig authConfig = new OAuthConfig(env.getProperty("netsuite.consumer.key"), env.getProperty("netsuite.consumer.secret"));
		OAuth1AccessToken token = new OAuth1AccessToken(env.getProperty("netsuite.token.key"), env.getProperty("netsuite.token.secret"));
		String url  = env.getProperty("netsuite.baseurl")+"script="+scriptNum+"&deploy="+deployNum+"&recordtype="+dataType.getRecordName()+"&id="+0+"&isMultiple="+true;
		try (OAuth10aService auth10aService = new OAuth10aService(new NSApi(), authConfig)) {
			OAuthRequest request = new OAuthRequest(Verb.GET, url);
			request.setRealm("TSTDRV1798497");
			request.addHeader("Content-Type", "application/json");
			request.addHeader("Accept", "application/json");
			auth10aService.signRequest(token, request);
			Response response = auth10aService.execute(request);
			String body = StreamUtils.getStreamContents(response.getStream());
			logger.info(body);
			return  DataHelper.processNSResponseForType(body,dataType);
		}
		catch(Exception e) {
			logger.error("Error Calling NetSuite API",e);
		}
		return getMessage(failureMessage);
	}


	class NSApi extends DefaultApi10a {

		@Override
		public String getRequestTokenEndpoint() {
			return null;
		}

		@Override
		public String getAccessTokenEndpoint() {
			return null;
		}

		@Override
		public String getAuthorizationUrl(OAuth1RequestToken requestToken) {
			return null;
		}

	}






	public boolean createNsBillPayment(MakePaymentModalView makePayment) {
		OAuthConfig authConfig = new OAuthConfig(env.getProperty("netsuite.consumer.key"), env.getProperty("netsuite.consumer.secret"));
		OAuth1AccessToken token = new OAuth1AccessToken(env.getProperty("netsuite.token.key"), env.getProperty("netsuite.token.secret"));
		String url  = env.getProperty("netsuite.baseurl")+"script="+scriptNum+"&deploy="+deployNum;
		try (OAuth10aService auth10aService = new OAuth10aService(new NSApi(), authConfig)) {
			OAuthRequest request = new OAuthRequest(Verb.POST, url);
			request.setRealm("TSTDRV1798497");

			request.addHeader("Content-Type", "application/json");
			request.addHeader("Accept", "application/json");



			NsMakePayment nsPayObj = new NsMakePayment();
			nsPayObj.setAmountPaid(makePayment.getPaymentAmount().toString());
			//nsPayObj.setTxnDate(makePayment.getPaymentDate().toString());
			//nsPayObj.setPaymentType( BillPaymentTypeEnum.CHECK.toString());
			nsPayObj.setNotes(makePayment.getNote());
			nsPayObj.setVendorName(makePayment.getVendorId());
			nsPayObj.setExchangerate("1.00");
			nsPayObj.setCurrency("1");

			/*// add bill payment with minimum mandatory fields
			payment.setPaymentAmount(billObj.getBalance());
			billPayment.setVendorRef(billObj.getVendorRef());
			billPayment.setTxnDate(payment.getPaymentDate());
			billPayment.setPrivateNote(payment.getNote());
			billPayment.setTotalAmt(payment.getPaymentAmount());
			BillPaymentTypeEnum paymentType = BillPaymentTypeEnum.CHECK; //TODO harcoded
			if(paymentType.equals(BillPaymentTypeEnum.CHECK)){
				billPayment.setCheckPayment(getCheckData(service));
			}
			else{
				billPayment.setCreditCardPayment(getCreditCardData(service));
			}
			billPayment.setPayType(paymentType);
			//linked transaction for bill 
			 */			request.setPayload("{\"message\" : \"Hello World\"}"); //"&recordtype="+NS_BILL_PAYMENT.getRecordName()+"&id="+0+"&isMultiple="+true;
			 System.out.println("PAYLOAD:");
			 System.out.println(request.getStringPayload());
			 auth10aService.signRequest(token, request);
			 Response response = auth10aService.execute(request);
			 String body = StreamUtils.getStreamContents(response.getStream());
			 logger.info(body);
			 return true;
		}
		catch(Exception e) {
			logger.error(MessageEnum.UNABLE_PAYMENT_ERROR.desc() +e.getMessage());
			return false;
		}


	}













}
