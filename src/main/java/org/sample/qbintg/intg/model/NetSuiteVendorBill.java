package org.sample.qbintg.intg.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
@JsonIgnoreProperties(ignoreUnknown = true)
public class NetSuiteVendorBill implements Serializable {

	private static final long serialVersionUID  =1L;

	private String txnNo;

	@JsonProperty("values")
	private Data data;
	
	public Data getData() {
		return data;
	}

	@Override
	public String toString() {
		return "NetSuiteVendorBill [txnNo=" + txnNo + ", data=" + data.toString() + "]";
	}


	@JsonProperty("id")
	public String getTxnNo() {
		return txnNo;
	}
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Data{
		private String txnDate;
		private String payee;
		private String category;
		private String dueDate;
		private String balance;
		private String total;
		private String amount;
	
		@JsonProperty("trandate")
		public String getTxnDate() {
			return txnDate;
		}

		@JsonProperty("vendor.entityid")
		public String getPayee() {
			return payee;
		}

		@JsonProperty("expenseCategory.description")
		public String getCategory() {
			return category;
		}

		@JsonProperty("duedate")
		public String getDueDate() {
			return dueDate;
		}

		@JsonProperty("amountremaining")
		public String getBalance() {
			return balance;
		}

		@JsonProperty("total")
		public String getTotal() {
			return total;
		}
		
		@JsonProperty("amount")
		public String getAmount() {
			return amount;
		}
		

		@Override
		public String toString() {
			return "Data [txnDate=" + txnDate + ", payee=" + payee + ", category=" + category + ", dueDate=" + dueDate + ", amount=" + amount
					+ ", balance=" + balance + ", total=" + total + "]";
		}
	
		
		
	}

}