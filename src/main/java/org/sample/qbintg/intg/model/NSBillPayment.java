package org.sample.qbintg.intg.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
@JsonIgnoreProperties(ignoreUnknown = true)

/**
 * {
    "id": "7610",
    "values": {
      "trandate": "12\/14\/2014",
      "statusref": [

      ],
      "total": "-3000.00",
      "transactionnumber": "2010",
      "postingperiod": [
        {
          "value": "255",
          "text": "Dec 2015"
        }
      ],
      "amountunbilled": "",
      "amount": "-3000.00",
      "amountpaid": ".00",
      "amountremaining": "",
      "vendor.entityid": "Alexander Valley Vineyards",
      "approvalstatus": [

      ],
      "billeddate": "",
      "entitystatus": [

      ],
      "creditamount": "3000.00",
      "netamount": "-3000.00",
      "accounttype": "Bank"


    }
 */
public class NSBillPayment implements Serializable {
	private static final long serialVersionUID  =1L;

	private String billPaymentId; //id

	@JsonProperty("values")
	private Data data;

	public Data getData() {
		return data;
	}

	@Override
	public String toString() {
		return "NetSuiteVendorBill [billPaymentId=" + billPaymentId + ", data=" + data.toString() + "]";
	}

	@JsonProperty("id")
	public String getBillPaymentId() {
		return billPaymentId;
	}


	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Data{
		
		private String vendorName; 
		private String txnDate;
		private String paymentType; //accounttype
		private String amountPaid; //amountpaid
		private String billTxnId; //TODO hardcoded
		
		@JsonProperty("vendor.entityid")
		public String getVendorName() {
			return vendorName;
		}


		@JsonProperty("accounttype")
		public String getPaymentType() {
			return paymentType;
		}

		@JsonProperty("amountpaid")
		public String getAmountPaid() {
			return amountPaid;
		}

		@JsonProperty("transactionnumber")
		public String getBillTxnId() {
			return billTxnId;
		}


		@JsonProperty("trandate")
		public String getTxnDate() {
			return txnDate;
		}


		@Override
		public String toString() {
			return "Data [vendorName=" + vendorName + ", txnDate=" + txnDate + ", paymentType=" + paymentType
					+ ", amountPaid=" + amountPaid + ", billTxnId=" + billTxnId + "]";
		}

	}

}