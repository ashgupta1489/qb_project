package org.sample.qbintg.intg.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PaymentResponse implements Serializable {

	@JsonIgnore
	private static final long serialVersionUID  =1L;


	private String processedTimeStamp;
	private String status;
	private String error;
	private String serverUUID;
	private String clientUUID;
	@JsonProperty("notes")
	private String serverNotes;

	private String serverTimestamp;
	
	public PaymentResponse() {
		
	}
	public PaymentResponse(VendorPayment vendorPayment) {
		if(vendorPayment==null) {
			throw new IllegalArgumentException("Null Arguments");
		}
		this.status= vendorPayment.getStatus();
		this.error= vendorPayment.getError();
		this.serverUUID = vendorPayment.getServerUUID();
		this.clientUUID= vendorPayment.getClientUUID();
		this.serverNotes = vendorPayment.getServerNotes();
		this.serverTimestamp = LocalDateTime.now().toString();
		this.processedTimeStamp= vendorPayment.getProcessedTimeStamp()!=null?vendorPayment.getProcessedTimeStamp().toString():"N/A";
	}
	
	public String getServerTimestamp() {
		return serverTimestamp;
	}
	public void setServerTimestamp(String serverTimestamp) {
		this.serverTimestamp = serverTimestamp;
	}
	public String getProcessedTimeStamp() {
		return processedTimeStamp;
	}

	public void setProcessedTimeStamp(String processedTimeStamp) {
		this.processedTimeStamp = processedTimeStamp;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getServerUUID() {
		return serverUUID;
	}

	public void setServerUUID(String serverUUID) {
		this.serverUUID = serverUUID;
	}

	public String getClientUUID() {
		return clientUUID;
	}

	public void setClientUUID(String clientUUID) {
		this.clientUUID = clientUUID;
	}

	public String getServerNotes() {
		return serverNotes;
	}

	public void setServerNotes(String serverNotes) {
		this.serverNotes = serverNotes;
	}

	
}