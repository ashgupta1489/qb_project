package org.sample.qbintg.intg.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
@JsonIgnoreProperties(ignoreUnknown = true)
public class NsVendor implements Serializable {

	private static final long serialVersionUID  =1L;

	private String vendorId;
	
	@JsonProperty("values")
	private Data data;
	
	public Data getData() {
		return data;
	}

	@Override
	public String toString() {
		return "NetSuiteVendorBill [vendorId=" + vendorId + ", data=" + data.toString() + "]";
	}

	@JsonProperty("id")
	public String getVendorId() {
		return vendorId;
	}


	

	
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class Data{
		private String email;
		private String companyName;
		private String accountNum;
//		private String routingNum;
		private String balance;
		private String phone;
		
		@JsonProperty("email")
		public String getEmail() {
			return email;
		}
		
		@JsonProperty("entityid")
		public String getCompanyName() {
			return companyName;
		}
		
		@JsonProperty("accountnumber")
		public String getAccountNum() {
			return accountNum;
		}
		
		@JsonProperty("balance")
		public String getBalance() {
			return balance;
		}
		
		@JsonProperty("phone")
		public String getPhone() {
			return phone;
		}
		@Override
		public String toString() {
			return "Data [email=" + email + ", companyName=" + companyName + ", accountNum=" + accountNum + ", balance="
					+ balance + ", phone=" + phone + "]";
		}
		
		
	}

}
