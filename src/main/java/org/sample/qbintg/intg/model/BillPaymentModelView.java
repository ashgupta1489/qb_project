package org.sample.qbintg.intg.model;

import static org.sample.qbintg.intg.util.Utilities.toCurrency;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.codehaus.plexus.util.StringUtils;
import org.sample.qbintg.intg.util.Utilities;

import com.intuit.ipp.data.BillPayment;
import com.intuit.ipp.data.Line;
import com.intuit.ipp.data.LinkedTxn;

public class BillPaymentModelView implements Serializable{

	private static final long serialVersionUID  =1L;
	private String billPaymentId;
	private String vendorName;
	private String txnDate;
	private String paymentType;
	private String amountPaid;
	private String billTxnId;
	private String source;

	private Predicate<LinkedTxn> nonNullPredicate = Objects::nonNull;
	private Predicate<LinkedTxn> typeNotNull = p -> p.getTxnType() != null;
	private Predicate<LinkedTxn> billType = p -> p.getTxnType().equals("Bill");
	private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy hh:mm a");
	
	public BillPaymentModelView(BillPayment billPayment){
		if(billPayment==null){
			throw new IllegalArgumentException("Wrong or null input");
		}
		this.paymentType= billPayment.getPayType().value();
		this.billPaymentId = StringUtils.isNotEmpty(billPayment.getId())?billPayment.getId():"Void";
		this.vendorName=billPayment.getVendorRef().getName();
		
		this.txnDate = Utilities.convertDateToStringWithAMPM(billPayment.getTxnDate());
		this.amountPaid = Utilities.toCurrency(billPayment.getTotalAmt(), billPayment.getCurrencyRef().getValue());
		Predicate<LinkedTxn> fullPredicate = nonNullPredicate.and(typeNotNull).and(billType);
		StringBuffer billTransId = new StringBuffer();
		for ( Line line : billPayment.getLine()) {
			billTransId.append(line.getLinkedTxn().stream().filter(fullPredicate).
					map(s -> s.getTxnId()).collect(Collectors.joining(",")));
		}
		this.billTxnId = StringUtils.isNotEmpty(billTransId.toString())?billTransId.toString():"Void";
		this.source="Pulled from QuickBooks";
	}
	
	public BillPaymentModelView(VendorPayment vendorPayment){
		if(vendorPayment==null){
			throw new IllegalArgumentException("Wrong or null input");
		}
		this.paymentType= vendorPayment.getAccountType();
		this.billPaymentId = vendorPayment.getServerUUID();
		this.vendorName=vendorPayment.getVendorName();
		this.txnDate = vendorPayment.getPostedTimeStamp().format(formatter);
		this.amountPaid = Utilities.toCurrency(vendorPayment.getPostedAmount());
		this.billTxnId = "N/A";
		this.source="Paid From NetSuite";
	}

	
	
	
	DateFormat originalFormat = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
	public BillPaymentModelView(NSBillPayment record) {
		if(record==null){
			throw new IllegalArgumentException("Wrong or null input");
		}
		this.source="Pulled from NetSuite";
		this.billPaymentId = record.getBillPaymentId();
		this.vendorName= record.getData().getVendorName();
		this.amountPaid = toCurrency(record.getData().getAmountPaid(),"USD");
		this.paymentType = record.getData().getPaymentType();
		Date tempData= null;
		try {
			tempData = originalFormat.parse(record.getData().getTxnDate());
		}
		catch(Exception e) {
			System.err.println("Error While Converting Record "+e);
		}
		this.txnDate = Utilities.convertDateToStringWithAMPM(tempData);
		this.billTxnId = record.getData().getBillTxnId();

	}



	public String getBillPaymentId() {
		return billPaymentId;
	}

	public String getPaymentType() {
		return paymentType;
	}
	public String getVendorName() {
		return vendorName;
	}
	public String getAmountPaid() {
		return amountPaid;
	}
	public String getTxnDate() {
		return txnDate;
	}
	public String getBillTxnId() {
		return billTxnId;
	}



	public String getSource() {
		return source;
	}

}
