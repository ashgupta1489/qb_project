package org.sample.qbintg.intg.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
@Entity
@Table(name="payment")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value={"serialVersionUID","erpSystem","sysVendorId","postedAmount","createdTimestamp","postedTimeStamp","updatedTimestamp"
							,"status","error","serverNotes","sysVendorId","postedAmount"
							})
@JsonInclude(Include.NON_NULL)
public class VendorPayment  implements Serializable{
	private static final long serialVersionUID  =1L;

	@JsonProperty("reqTimestamp")
	@Transient
	private String postedTimeStampUnit;
	
	@Column(name = "created_timestamp", updatable = false, nullable = false)
	//@Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
	private LocalDateTime createdTimestamp;
	
	
	@Column(name = "updated_timestamp")
	//@Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
	private LocalDateTime updatedTimestamp;
	
	
	@Column(name = "posted_timestamp", updatable = false, nullable = false)
//	@Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
	private LocalDateTime postedTimeStamp;
	
	@Column(name = "processed_timestamp")
	//@Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
	private LocalDateTime processedTimeStamp;
	
	
	@Id
	@JsonProperty("sequence_num")
    @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "payment_id", updatable = false, nullable = false)
	private int id;

	@Column(name = "account_num",length=20, nullable=false)
	private String accountNumber;
	
	@Column(name = "routing_num",length=20, nullable=false)
	private String routingNumber;

	
	@Column(name = "account_name",length=20, nullable=false)
	private String accountName;
	
	@Column(name = "account_type",length=20, nullable=false)
	private String accountType;
	
	@Column(name = "currency",length=20, nullable=false)
	private String currency;
	
	@Column(name = "erp_system",length=20, nullable=false)
	private String erpSystem;
	
	@Column(name = "initiator_id",length=20)
	private String initiatorUserId;
	
	@Column(name = "payment_status",length=20)
	private String status;
	
	@Column(name = "payment_error",length=200)
	private String error;
	
	@JsonProperty("s_uuid")
	@Column(name = "server_uuid",length=50, nullable=false,unique=true)
	private String serverUUID;
	
	@JsonProperty("c_uuid")
	@Column(name = "client_uuid",length=50, nullable=false)
	private String clientUUID;

	@Column(name = "server_notes",length=200)
	private String serverNotes;
	
	@JsonProperty("notes")
	@Column(name = "client_notes",length=200)
	private String clientNotes;
	
	@Column(name = "vendor_name",length=50, nullable=false)
	private String vendorName;
	
	@JsonProperty("vendorId")
	@Column(name = "erp_vendor_id",length=50, nullable=false)
	private String vendorRecId;
	
	@Column(name = "vendor_id")
	private int sysVendorId;
	
	
	@Column(name = "amount", nullable=false,precision=2, length=15)
	private BigDecimal postedAmount;

	@Transient
	private BigDecimal amount;
	
	
	
	
	
	
	
	
	

	@Override
	public String toString() {
		return "VendorPayment [postedTimeStampUnit=" + postedTimeStampUnit + ", createdTimestamp=" + createdTimestamp
				+ ", updatedTimestamp=" + updatedTimestamp + ", postedTimeStamp=" + postedTimeStamp
				+ ", processedTimeStamp=" + processedTimeStamp + ", id=" + id + ", accountNumber=" + accountNumber
				+ ", routingNumber=" + routingNumber + ", accountName=" + accountName + ", accountType=" + accountType
				+ ", currency=" + currency + ", erpSystem=" + erpSystem + ", initiatorUserId=" + initiatorUserId
				+ ", status=" + status + ", error=" + error + ", serverUUID=" + serverUUID + ", clientUUID="
				+ clientUUID + ", serverNotes=" + serverNotes + ", clientNotes=" + clientNotes + ", vendorName="
				+ vendorName + ", vendorRecId=" + vendorRecId + ", sysVendorId=" + sysVendorId + ", postedAmount="
				+ postedAmount + ", amount=" + amount + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((serverUUID == null) ? 0 : serverUUID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VendorPayment other = (VendorPayment) obj;
		if (id != other.id)
			return false;
		if (serverUUID == null) {
			if (other.serverUUID != null)
				return false;
		} else if (!serverUUID.equals(other.serverUUID))
			return false;
		return true;
	}

	public String getPostedTimeStampUnit() {
		return postedTimeStampUnit;
	}

	public void setPostedTimeStampUnit(String postedTimeStampUnit) {
		this.postedTimeStampUnit = postedTimeStampUnit;
	}

	public LocalDateTime getCreatedTimestamp() {
		return createdTimestamp;
	}

	public void setCreatedTimestamp(LocalDateTime createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public LocalDateTime getUpdatedTimestamp() {
		return updatedTimestamp;
	}

	public void setUpdatedTimestamp(LocalDateTime updatedTimestamp) {
		this.updatedTimestamp = updatedTimestamp;
	}

	public LocalDateTime getPostedTimeStamp() {
		return postedTimeStamp;
	}

	public void setPostedTimeStamp(LocalDateTime postedTimeStamp) {
		this.postedTimeStamp = postedTimeStamp;
	}

	public LocalDateTime getProcessedTimeStamp() {
		return processedTimeStamp;
	}

	public void setProcessedTimeStamp(LocalDateTime processedTimeStamp) {
		this.processedTimeStamp = processedTimeStamp;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getRoutingNumber() {
		return routingNumber;
	}

	public void setRoutingNumber(String routingNumber) {
		this.routingNumber = routingNumber;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getErpSystem() {
		return erpSystem;
	}

	public void setErpSystem(String erpSystem) {
		this.erpSystem = erpSystem;
	}

	public String getInitiatorUserId() {
		return initiatorUserId;
	}

	public void setInitiatorUserId(String initiatorUserId) {
		this.initiatorUserId = initiatorUserId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getServerUUID() {
		return serverUUID;
	}

	public void setServerUUID(String serverUUID) {
		this.serverUUID = serverUUID;
	}

	public String getClientUUID() {
		return clientUUID;
	}

	public void setClientUUID(String clientUUID) {
		this.clientUUID = clientUUID;
	}

	public String getServerNotes() {
		return serverNotes;
	}

	public void setServerNotes(String serverNotes) {
		this.serverNotes = serverNotes;
	}

	public String getClientNotes() {
		return clientNotes;
	}

	public void setClientNotes(String clientNotes) {
		this.clientNotes = clientNotes;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public String getVendorRecId() {
		return vendorRecId;
	}

	public void setVendorRecId(String vendorRecId) {
		this.vendorRecId = vendorRecId;
	}

	public int getSysVendorId() {
		return sysVendorId;
	}

	public void setSysVendorId(int sysVendorId) {
		this.sysVendorId = sysVendorId;
	}

	public BigDecimal getPostedAmount() {
		return postedAmount;
	}

	public void setPostedAmount(BigDecimal postedAmount) {
		this.postedAmount = postedAmount;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	
	
	
	
}
