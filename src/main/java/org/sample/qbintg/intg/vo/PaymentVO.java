package org.sample.qbintg.intg.vo;

import java.io.Serializable;

public class PaymentVO implements Serializable{
	private static final long serialVersionUID = 1L;
	private String postedTimeStamp;
	private String accountNum;
	private String routingNum;
	private String accountName;
	private String initiatorUserId;
	private String status;
	private String vendorName;
	private String  postedAmount;
	
	
	
	
	@Override
	public String toString() {
		return "PaymentVO [postedTimeStamp=" + postedTimeStamp + ", accountNum=" + accountNum + ", routingNum="
				+ routingNum + ", accountName=" + accountName + ", initiatorUserId=" + initiatorUserId + ", status="
				+ status + ", vendorName=" + vendorName + ", postedAmount=" + postedAmount + "]";
	}
	public String getPostedTimeStamp() {
		return postedTimeStamp;
	}
	public void setPostedTimeStamp(String postedTimeStamp) {
		this.postedTimeStamp = postedTimeStamp;
	}
	public String getAccountNum() {
		return accountNum;
	}
	public void setAccountNum(String accountNum) {
		this.accountNum = accountNum;
	}
	public String getRoutingNum() {
		return routingNum;
	}
	public void setRoutingNum(String routingNum) {
		this.routingNum = routingNum;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getInitiatorUserId() {
		return initiatorUserId;
	}
	public void setInitiatorUserId(String initiatorUserId) {
		this.initiatorUserId = initiatorUserId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getVendorName() {
		return vendorName;
	}
	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}
	public String getPostedAmount() {
		return postedAmount;
	}
	public void setPostedAmount(String postedAmount) {
		this.postedAmount = postedAmount;
	}
	
	
	/*	private String serverUUID;
	private String clientUUID;
	private String serverNotes;
	private String clientNotes;*/

}
