package org.sample.qbintg.repository;

import java.util.List;

import org.sample.qbintg.intg.model.VendorPayment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VendorPaymentDataRepository extends JpaRepository<VendorPayment, Long>,IDataRepository  {

	VendorPayment findByServerUUID(final String serverUUID);
	List<VendorPayment> findAllByOrderByIdAsc();
	List<VendorPayment> findByServerUUIDIn(List<Long> serverUUIDs);
}